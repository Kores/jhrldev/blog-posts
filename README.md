# blog-posts

All [jhrl.dev](https://jhrl.dev) posts in markdown.

## Original posts

All original posts can be found in the `posts/` directory.

## Post-processed markdown

Post-processed markdown files can be found in the `md-out/` directory. 
Beware that post-processed files may not be up-to-date, as they are currently manually
built using a `pre-hook`, which may not be configured in some environments that I use to edit, like
my smartphone.

...
