<p style="text-align: left; font-size: x-large;">
    <a class="header" href="about_me.html">
        <span id="author-name">Jonathan Lopes</span>
    </a>
    <span id="publication-date" style="float:right;">
        <a href="https://github.com/JonathanxD" target="_blank">
            <img src="main-author.png" alt="Author" style="border-radius: 50%; width: 128px; vertical-align: middle;"/>
        </a>
    </span>
</p>

Desenvolvedor Rust, Kotlin, Java, Scala, Groovy, Go, Lua e especialista em JVM Bytecode. 
Criador do [KoresFramework](), [Redin](), [KWCommands]() e uma série de outros projetos desconhecidos. 

Atualmente focado nos meus projetos Rust e contribuições para a comunidade.

#### Linha do tempo

<div class="timeline">
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2010</span>

Comecei a programar em [Lua]() e [C/C++](), usando [AutoplayMediaStudio]() e [Dev C++]().

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2012</span>

Aprendi [Pawn-gh]() e fui dono de um servidor de [GTA:SA-MP]() até 2014.

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2014</span>

Aprendi [Java]() e comecei a fazer mods para o Minecraft usando o [ForgeMC](), e plugins utilizando [Bukkit]() para clientes privados,
além disso, iniciei o meu canal no [YouTube]() ensinando programação e desenvolvimento de plugins [Bukkit]().

Entrei no fórum [AtomGamers]() como Diretor 
e criei uma série de postagens e vídeos ensinando [Java]() e programação em geral.

Estudei [Scala](), [Groovy]() e escrevi modificações client e server-side com ambas linguagens.

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2015</span>

Entrei no fórum [BlackHubOS]() como Diretor. Uma tentativa do original fundador do [AtomGamers]() de reviver
a comunidade.

Aqui escrevi algumas postagens sobre Linux e minhas modificações.

Esse também foi o ano que descobri a maravilhosa [linguagem Rust|rust-1.0-launch]() --- o mesmo ano que a primeira versão
foi lançada (1.0.0) --- e comecei a aprender os básicos da linguagem.
</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2016</span>

Fiz meu primeiro [commit](https://github.com/koresframework/Kores/commit/c1ac85f859eb2ce6621420fde86e087fae849c10) do 
[KoresFramework](), projeto que nomeie como `CodeAPI` no início.

Desde então, foquei em aprender os detalhes internos de compiladores e da [Java Virtual Machine](),
e fazer engenharia reversa do [Java Compiler](), o que me permitiu implementar o backend inteiro do compilador
sob a licença MIT[^compiler-backend].

E no mesmo ano, eu traduzi a base do código em [Java]() do [KoresFramework]() 
para [Kotlin]() (daí o prefixo `K` no nome)[^port].

[^compiler-backend]: O [KoresFramework]() tem dois backends, um que gera código [Java](https://gitlab.com/Kores/Kores-SourceWriter)
e um que gera [código de máquina pronto para executar na JVM com zero dependências](https://gitlab.com/Kores/Kores-BytecodeWriter).
Por implementar apenas o backend significa basicamente que o [KoresFramework](), 
por si só, não pode compilar código fonte Java (trabalho do frontend).

[^port]: [commit](https://github.com/koresframework/Kores/commit/386f27dd6cb34cb2e9a7d18e778d4c28329211f4).

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2018</span>

Iniciei minha própria linguagem de programação usando [Antlr4]() para análise
léxica, sintática e semântica, e o [KoresFramework]() para validação e compilação.

Porém, como eu estava programando tanto a infraestrutura do compilador e a linguagem, apenas produzi
uma versão extremamente experimental com alguns recursos como variáveis, métodos e lambdas, porém nunca
publiquei, pois ainda haviam muitos problemas para resolver no _backend_ de [código de máquina JVM|jvm-bytecode]().
E também por estar extremamente animado com o fato de conseguir implementar o meu próprio compilador Java,
que estava funcionando depois de sofrer muito para implementar as coisas corretamente.

*E por isso ter levado 4 anos contínuos de estudo intenso em Java e na JVM, agora entendo a maioria
dos mecanismos envolvidos na execução do [código JVM|jvm-bytecode]() e como as coisas são implementadas debaixo do capô,
e também como elas são piores do que eu imaginava. Uma citação honrosa vai para os genéricos, mas tem coisas boas
também, por exemplo, como funciona o invokedynamic, que você pode ver em 
[artigo meu no medium](https://jhrl.medium.com/java-o-poderoso-invokedynamic-e-pattern-matching-jep-406-5610b9ec81ca).*

*Hoje eu posso dizer que sou completamente capaz de implementar uma linguagem nova para a JVM do zero,
como também que entendo claramente as implicações técnicas e de desempenho de cada decisão, por isso dei-me
o título de **Especialista em JVM Bytecode**.*

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2020</span>

Depois de 4 anos de desenvolvimento, eu finalmente consegui implementar um gerador de [código de máquina JVM|jvm-bytecode]()
totalmente funcional, o [Kores-BytecodeWriter](), podendo gerar código executável e com zero dependências direto
da estrutura de árvores do [Kores]().

No meio desse processo, desenvolvi vários outros projetos enquanto aprendia os diferentes aspectos da [Java Virtual Machine](),
um desses projetos foi [um proxy especializado com despacho estático|KoresProxy](), para reduzir o _overhead_ 
dos Proxies Java nativos em cenários com frequentes chamadas de métodos. Por suportar despacho estático, isso faz desses Proxies
tão rápidos quanto código escrito a mão (ou Proxies escritos com [ByteBuddy]() ou [cglib](), por exemplo).

Nesse mesmo ano comecei também a trabalhar em uma empresa e ir a faculdade, então não tive muito tempo para tocar meus projetos,
e agora, após todos esses, eles acabaram um pouco abandonados, apesar da maioria estarem praticamente completos.

Se você tiver mais interesse na história do [KoresFramework](), o motivo por eu ter criado ele, as dificuldades e seu objetivo,
leia meu artigo sobre [A história do Kores](./kores-story.md).

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2021</span>

Comecei a aprender mais e mais sobre [Rust]() e consumir conteúdos diariamente sobre a linguagem.

No meio do ano, comecei a trabalhar profissionalmente com Go, uma linguagem de programação que estou
familiarizado, mas nunca foquei muito em aprender, mas foi muito fácil me tornar produtivo com ela,
já que eu sabia todos conceitos básicos.

No final do ano, depois do primeiro _deploy_ experimental do [idocs]() (projeto qual se encontra pausado),
que foi completamente escrito em Kotlin usando o _framework_ Ktor e Corrotinas, não toquei mais
nenhum código Java/Kotlin por meses.

Próximo ao final do ano, fui contratado por um cliente particular como especialista Java/Kotlin e Head de Tecnologia.
Dei apoio ao sistema legado e liderei mudanças estruturais e pequenas modernizações para 
manter a plataforma funcional para os dois maiores clientes deles. Após o fim do contrato, deixei
Java/Kotlin novamente e foquei em Rust e Go.

Também publiquei uma [Biblioteca de detecção bináriai](https://crates.io/crates/bindet) 
e um [Cliente Open-Source para VPN AWS](https://crates.io/crates/openaws-vpn-client), por meio de engenharia reversa
e recursos esparsos na internet, ambos publicados no [crates.io]().
</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2022</span>

Esse ano publiquei o [kravltree](https://crates.io/crates/kravltree) 
e o [junitify](https://crates.io/crates/junitify), e agora estou focando em produzir
mais conteúdos sobre programação e tecnologia, além de estudos de computação gráfica com [Vulkan]().
</div>
</div>
<div class="timeline-container timeline-right timeline-deadend">
<div class="timeline-deadend-content"></div>
</div>
</div>

Convido-te a dar uma olhada no meu perfil do [GitHub](https://github.com/JonathanxD) se quiser
saber um pouquinho mais das tecnologias que eu já trabalhei. Sim, já trabalhei com muitas.

### <i class="fa fa-hashtag"></i> Social

- [GitHub](https://github.com/JonathanxD)
- [GitLab](https://gitlab.com/jhrl)
- [Twitter](https://twitter.com/jonathanhrll)
- [Medium](https://jhrl.medium.com)
