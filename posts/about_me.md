<p style="text-align: left; font-size: x-large;">
    <a class="header" href="about_me.html">
        <span id="author-name">Jonathan Lopes</span>
    </a>
    <span id="publication-date" style="float:right;">
        <a href="https://github.com/JonathanxD" target="_blank">
            <img src="main-author.png" alt="Author" style="border-radius: 50%; width: 128px; vertical-align: middle;"/>
        </a>
    </span>
</p>

Rust, Kotlin, Java, Scala, Groovy, Go, Lua developer and JVM Bytecode specialist. Creator of [KoresFramework](), [Redin](), [KWCommands]()
and a bunch of other (never known) projects. 

Now focused on my Rust and content production projects.

#### Timeline

<div class="timeline">
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2010</span>

Started programming in [Lua]() and [C/C++](), using [AutoplayMediaStudio]() and [Dev C++]().

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2012</span>

Learned [Pawn-gh]() and owned a [GTA:SA-MP]() server until 2014.

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2014</span>

Learned [Java]() and started making [ForgeMC]() Minecraft Mods and [Bukkit]() plugins for private clients,
also started my first [YouTube]() channel to teach how to create [Bukkit]() plugins.

Also, I joined [AtomGamers]() Forum as a Director 
and created a series of video tutorials and posts teaching [Java]()
and programming in general.

And studied about [Scala]() and [Groovy](), and made some mods and plugins with them.

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2015</span>

I joined [BlackHubOS]() as Director. This project was an attempt of the original founder of [AtomGamers]() to revive
the community. 

Here I made some posts about Linux and my own plugins.

This was also the year that I also discovered the [Rust Language|rust-1.0-launch]() --- the same year that the first version
landed --- and I started learning the basics of the language.
</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2016</span>

I made the [first commit](https://github.com/koresframework/Kores/commit/c1ac85f859eb2ce6621420fde86e087fae849c10) of [KoresFramework](), which was called `CodeAPI` by that time.

Since then, I focused on learning the deep details of compilers and the [Java Virtual Machine](), and doing reverse engineering of
[Java Compiler]().

And in this same year, I translated the [Java]() code base of [KoresFramework]() to [Kotlin]() (hence the `K` prefix in the new name)[^port].

[^port]: [the commit](https://github.com/koresframework/Kores/commit/386f27dd6cb34cb2e9a7d18e778d4c28329211f4).

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2018</span>

I started writing my own programming language using [Antlr4]() and [KoresFramework]() to parse, validate and compile it.

But as I was programming the compiler infrastructure and the language, I only produced a very experimental
version of it, but never published it because there were a bunch of bugs to fix in the Bytecode generator, and
I was **very** excited by the fact that I was implementing my own Java compiler, and it was working after a bunch of
struggle in getting things right.

*And, because that I actually took 4 years of intense Java and JVM study, now I understand most of
the mechanism involved in the code execution and how things are implemented under-the-hood,
and how most of them are worse than I thought.*

*I'm completely able to implement an entire new programming language for the JVM, and I understand the performance
and technical implications of my choices, so I gave myself the **JVM Bytecode Specialist** title.*

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2020</span>

After 4 years of development, I managed to implement an entirely working intermediary Bytecode generator,
the [Kores-BytecodeWriter](), it's like a [Java Compiler](), but without the parser machine, only the translation
mechanism from a Tree-like structure.

A bunch of projects was developed in the middle while I learned the different aspects of the [Java Virtual Machine](),
like [a specialized Proxy framework with support to static dispatch|KoresProxy](), to reduce the overhead of
Java Proxies in scenarios of frequent method invocation.

I also started working in a company and going to college, so I had not much time to touch my private projects, and, after
4 years, they ended up a bit abandoned.

</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2021</span>

I started learning more and more Rust and consuming more Rust content.

In the middle of the year I started working professionally with Go, a programming
language that I was familiarized, even though I never focused on learning Go,
it was a very easy to start being productive with it, since I already knew all the basics.

In the end of the year, after the first experimental deployment of [idocs](), 
which was written completely in Kotlin using Ktor
and Coroutines, I didn't touch any Java/Kotlin code for months.

Then a private client needed a specialist in Java/Kotlin to maintain their system
while the rest of the team was implementing another one (in other language), so I did the job,
and after the contract ended, I left the Java/Kotlin again and came back to focusing on Rust and Go.

I also published a [Binary detection Library](https://crates.io/crates/bindet) 
and an [Open-Source AWS VPN Client](https://crates.io/crates/openaws-vpn-client) to [crates.io]().
</div>
</div>
<div class="timeline-container timeline-right">
<div class="timeline-content">
<span class="timeline-title">2022</span>

This year I published my [kravltree](https://crates.io/crates/kravltree) 
and [junitify](https://crates.io/crates/junitify) crates, 
and I'm now focusing in producing more programming and technology content.
</div>
</div>
<div class="timeline-container timeline-right timeline-deadend">
<div class="timeline-deadend-content"></div>
</div>
</div>

I happily invite you to take a look in my [GitHub](https://github.com/JonathanxD) profile if you want to know
every bit of technologies that I know and have worked with. And yes, I've worked with a lot of them.


### <i class="fa fa-hashtag"></i> Social

- [GitHub](https://github.com/JonathanxD)
- [GitLab](https://gitlab.com/jhrl)
- [Twitter](https://twitter.com/jonathanhrll)
- [Medium](https://jhrl.medium.com)
