# Playground test

Testing Rust and Go playgrounds embedding.

## Rust playground

```rust,editable
fn main() {
    let x = 1 + 2;
    println!("{x}");
}
```

## Go playground

```go,editable
import (
    "fmt"
    "os"
)

func main() {
    x := 1 + 2;
    fmt.Println(x);
    fmt.Fprintln(os.Stderr, x);
}
```